---
layout: post
title:  "FISL17: compartilhar é um estilo de vida!"
desc: "A pessoa que compartilha seu conhecimento, aprende mais a cada compartilhamento."
keywords: "Eventos,FISL,Mozilla,Software Livre"
date:   2016-07-20
categories: [EVENTOS]
tags: [Eventos,FISL,Mozilla]
---
## *<center>“A pessoa que compartilha seu conhecimento, aprende mais a cada compartilhamento”</center>*

<br />

<center><img src="{{ site.img_path }}/eventos/fox.jpg" style="width:100%; max-width: 800px;"></center>

<center>
	Passeio com a Fox dentro do FISL17
</center>

<br />

Let's **Go!**

<br />

A décima sétima edição do Fórum Internacional de Software Livre ocorreu nos dias 13 a 16 de Julho deste ano em Porto Alegre, RS. Um dos maiores eventos de software livre do Brasil e mundo, onde diversas comunidades, empresas, pessoas e até mascotes se encontram pra disseminar seus resultados, projetos, tecnologias, ideias e muitos mais. Para mais detalhes, todas as palestras do FISL17 estão disponíveis, gravadas e disponibilizadas para todos na grade oficial do evento, <a href="http://agenda.fisl17.softwarelivre.org/#/" title="Grade oficial de atividades do FISL17" target="_blank">**neste link**</a>. Para acompanhar algumas das atividades nas eu quais participei em conjunto com a comunidade Mozilla Brasil, segue a listagem dos vídeos, (re)vejam:

<br />

<iframe src="https://player.vimeo.com/video/199871020?color=ff9933&byline=0" width="100%" height="375" style="max-width: 500px;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

<iframe src="https://player.vimeo.com/video/199886243?color=ff9933&byline=0" width="100%" height="375" style="max-width: 500px;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

<iframe src="https://player.vimeo.com/video/199895691?color=ff9933&byline=0" width="100%" height="375" style="max-width: 500px;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

<iframe src="https://player.vimeo.com/video/184115897?color=ff9933&byline=0" width="100%" height="375" style="max-width: 500px;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

<br />

Além das atividades apresentas no <a href="http://softwarelivre.org/fisl17/programacao/mozilla-brasil" title="Página do mini evento Mozilla Brasil no FISL17" target="_blank">Mini Evento Mozilla</a>, na área das comunidades ocorreram atividades sobre SUMO (Suporte Mozilla), MDN e mais — na qual podem conferir mais detalhes na <a href="https://blog.mozillabrasil.org.br/2016/07/mozilla-brasil-no-fisl17/" title="Resumo das atividades da Comunidade Mozilla Brasil no FISL17" target="_blank">postagem resumo no Blog da Comunidade Mozilla Brasil</a>, contendo um pouco sobre as atividades ocorridas no evento. Também a nossa linda mascote Fox alegrando todos os participantes ainda mais com suas fotos e danças.

<br />

<center><img src="{{ site.img_path }}/eventos/seg-privac.jpg" style="width:100%; max-width: 500px;"></center>

<center>
	Palestra sobre Segurança e Privacidade que contou com mais de 250 participantes
</center>

<br />

**1º dia**, 13/07/2016:

Logo na chegada ao evento, dando a abertura as atividades, no palco muito bem chamado de Ian Murdock (<a href="https://pt.wikipedia.org/wiki/Ian_Murdock" title="Wiki sobre Ian Murdock" target="_blank">fundador da distribuição GNU/Linux Debian</a>) com as palestras sobre como contribuir com o Fórum de Suporte e com a Base de Conhecimento, “apresentando o funcionamento e a importância dessas áreas funcionais para comunidade, principalmente como todos podem contribuir traduzindo, localizando, respondendo perguntas e auxiliando outros usuários dos produtos da Mozilla que chegam até o fórum.” — mais detalhes <a href="https://medium.com/@mozillabrasil/mozilla-brasil-no-fisl17-primeirodia-549edcd468ee#.xly7zdd87" title="Relato: primeiro dia de FISL17!" target="_blank">nesta postagem</a> no <a href="https://medium.com/@mozillabrasil" title="Medium da Comunidade Mozilla Brasil" target="_blank">medium da Mozilla Brasil</a>

<br />

<center><img src="{{ site.img_path }}/eventos/jaime-thiago.jpeg" title="Hands on: contribuindo com a Base de Conhecimento" alt="Hands on: contribuindo com a Base de Conhecimento" style="width:100%; max-width: 500px;"></center>

<center>
	Me and Thiago na palestra "Hands on: contribuindo com a Base de Conhecimento"
</center>

<br />

**2º dia**, 14/07/2016:

No segundo dia de evento, o mais importante para a comunidade Mozilla Brasil, onde ocorreram todas as atividades do <a href="http://softwarelivre.org/fisl17/programacao/mozilla-brasil" title="Página no Mini Evento Mozilla Brasil no FISL17" target="_blank">Mini Evento Mozilla</a>, atividades sobre <a href="http://hemingway.softwarelivre.org/fisl17/41f/sala41f-high-201607141811.ogv" title="Vídeo da Palestra sobre Connect Devices no FISL17" target="_blank">Connect Devices</a>, <a href="http://hemingway.softwarelivre.org/fisl17/41f/sala41f-high-201607141603.ogv" target="_blank">Segurança e Privacidade</a>, <a href="http://hemingway.softwarelivre.org/fisl17/41f/sala41f-high-201607141402.ogv" target="_blank">Como contribuir com a Comunidade Mozilla Brasil</a>, <a href="http://hemingway.softwarelivre.org/fisl17/41f/sala41f-high-201607141645.ogv" target="_blank">HTTP2</a> e <a href="http://agenda.fisl17.softwarelivre.org/#/2016-07-14" target="_blank">muito mais</a>.

<br />

<center><img src="{{ site.img_path }}/eventos/seguranca-e-privacidade.jpg" title="Palestra sobre Segurança e Privacidade" alt="Palestra sobre Segurança e Privacidade" style="width:100%; max-width: 500px;"></center>

<center>
	Eu + Nelson Dutra na Palestra sobre Segurança e Privacidade
</center>

<br />

**3º dia**, 15/07/2016:

<br />

<center><img src="{{ site.img_path }}/eventos/bancada.jpg" title="Bancada da Comunidade Mozilla Brasil no FISL17" alt="Bancada da Comunidade Mozilla Brasil no FISL17" style="width:100%; max-width: 500px;"></center>

<center>
	Bancada da comunidade Mozilla Brasil na Área das Comunidades do FISL17
</center>

<br />

No penúltimo dia de evento, apenas uma atividade foi aplicada, a “Contribuindo com o Exército dos Incríveis” — fora essa atividade, todas as outras foram feitas na bancada da comunidade Mozilla Brasil na área das comunidades do FISL17. Atividades como, sorteios pelo <a href="https://twitter.com/mozillabrasil" title="Twitter da Mozilla Brasil" target="_blank">twitter</a>, distribuição de brindes e muito mais. Nesse dia também conhecemos o Rogério “Fox” Pereira com sua busca implacável de 10 anos para conseguir uma camiseta do Firefox, comprando ou ganhando, mas a comunidade Mozilla Brasil deu um final feliz nessa história — <a href="https://www.facebook.com/mozillabrasil/videos/1196101153765300/" target="_blank">confira o vídeo</a> para mais detalhes sobre a história.

<br />

<center>	
	<blockquote class="instagram-media" data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/BH8irWDBj-4/" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A photo posted by Mozilla (@mozillagram)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2016-07-17T02:33:27+00:00">Jul 16, 2016 at 7:33pm PDT</time></p></div></blockquote>
	<script async defer src="//platform.instagram.com/en_US/embeds.js"></script>
</center>

<br />

**4º dia**, 16/07/2016:

É hora de dar tchau! No último dia do evento, e o mais triste, já deixando sua saudade ocorreram mais duas palestras rápidas, sobre o SUMO e Traduzindo com o Pontoon — também todos os membros da comunidade Mozilla Brasil e a mascote Fox fecharam o evento com toda sua alegria nos corredores do evento e na bancada da Mozilla na área das comunidades. Todos os participantes do evento se reuniram na área das comunidades para encerrar a 17ª edição do FISL, <a href="http://softwarelivre.org/articles/0157/6359/FISL17-numeros-finais.pdf" title="Números finais batidos pelo FISL17" target="_blank">apresentando todos os números, dados e resultados que o FISL17 gerou</a>. Alguns são:

* 3937 participantes
* 462 atividades na grade do evento
* 35 patrocinadores e apoiadores
* 13 comunidades
* participantes 16% mulheres e 84% homens
* palestrantes 15% mulheres e 85% homens
* voluntários 34% mulheres e 66% homens
* 21 países representados
* 72 caravanas
* 160 notícias no site do FISL
* 90 mil pessoas atingidas nas redes sociais do FISL
* 89.981 visualizações nas palestras online
* Pico de download alcançado: 208Mbps
* Total de dados trafegados: 1,2 Tb sendo 16% em IPv6
* E mais...


**Por fim, tivemos que dizer tchau ao FISL17 😕**

<br />

<center><img src="{{ site.img_path }}/eventos/fox-bye.jpg" title="Mascote Fox dando tchau ao FISL17" alt="Mascote Fox dando tchau ao FISL17" style="width:100%; max-width: 500px;"></center>

<center>
	Fox já com sua mala dando tchau ao FISL17
</center>

<br />

Na bancada da comunidade Mozilla Brasil na área das comunidades ocorretam diversas atividades como sorteio de brindes, desagios, fotos com a Fox, distribuição de kits especias da Mozilla e muito mais — a todos aqueles que ainda não participaram nenhum evento, não percam a oportunidade, eventos são investimentos profissionais e pessoais que só trazem benefícios para si — recomendo a leitura do texto <a href="https://medium.com/coderockr-way/por-que-ir-a-eventos-cd9fdf937c11#.qwp530yp3" title="Postagem: Por que ir a eventos?" target="_blank">“Por que ir a evento?”</a> do nosso amigo <a href="http://eltonminetto.net/about/" title="Sobre Elton Minetto" target="_blank">Elton Minetto</a>. Inspirem-se!

Obrigado a todos que de alguma forma ajudaram, acompanharam a minha participação no evento, enfim que fizeram o FISL17 acontecer. Muito obrigado mesmo, por tudo! <a href="https://wiki.mozilla.org/Brasil/FISL17" title="Wiki FISL17 Mozilla Brasil" target="_blank">Veja nossa wiki</a> sobre o evento e o <a href="https://wiki.mozilla.org/Brasil/FISL17/FISL_Playbook_v1.0" title="Playbook sobre o FISL17 e eventos em si" target="_blank">primeiro Playbook da comunidade Mozilla Brasil</a> sobre como organizar a presença da comunidade no FISL — mais relatos e outras postagens relacionadas a Mozilla e FISL vindas de outros meios e membros da comunidade Mozilla Brasil, você pode conferir através dos links:

* <a href="http://blog.polles.me/2016/07/21/mozilla-no-fisl17-momentos-de-um-mozillian-paranaense-em-poa/" title="Relato do Mozillian João Paulo Polles" target="_blank">Mozilla no FISL17 – Momentos de um Mozillian Paranaense em PoA</a>
* <a href="https://medium.com/@mozillabrasil/mozilla-brasil-no-fisl17-primeirodia-549edcd468ee#.4kq13rg19" title="Resumo das atividades da comunidade Mozilla Brasil no primeiro dia de evento" target="_blank">Mozilla Brasil no FISL17 #PRIMEIRODIA</a>
* <a href="http://softwarelivre.org/fisl17/noticias/oficina-apresenta-o-suporte-mozilla-aos-participantes-do-fisl17" title="Postagem sobre a oficina que apresenta o Suporte Mozilla aos participantes do FISL17 " target="_blank">Oficina apresenta o Suporte Mozilla aos participantes do FISL17</a>
* <a href="https://medium.com/@jhonatasrm/experi%C3%AAncia-fisl-17-fee1f7f3489a#.afvfw0qt5" title="Relato do Mozillian Jhonatas Rodrigues: experiência FISL17" target="_blank">Experiência FISL17</a>

## **NOS VEMOS NO FISL18 DE 5 A 8 DE JULHO DE 2017!**